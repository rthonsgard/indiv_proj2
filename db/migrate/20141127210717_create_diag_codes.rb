class CreateDiagCodes < ActiveRecord::Migration
  def change
    create_table :diag_codes do |t|
      t.string :diag_code
      t.string :description

      t.timestamps
    end
  end
end
