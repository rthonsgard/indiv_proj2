class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.datetime :app_date
      t.string :reason
      t.string :dr_notes
      t.integer :app_status_id
      t.integer :diag_code_id

      t.timestamps
    end
  end
end
