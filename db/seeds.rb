# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


AppStatus.create(status: 'Active')
AppStatus.create(status: 'Cancelled')

InvoiceStatus.create(status_name: 'Paid')
InvoiceStatus.create(status_name: 'Unpaid')


DiagCode.delete_all
open("lib/codes.txt") do |d_codes|
  d_codes.read.each_line do |code|
    diag_code, description = code.chomp.split("|")
    DiagCode.create!(:diag_code => diag_code, :description => description)
  end
end