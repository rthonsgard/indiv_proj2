Rails.application.routes.draw do

  get 'homepage/index'
  get '/appointments/doc_notes'
  get '/appointments/doc_notes_list'
  get '/appointments/doc_search'
  get '/appointments/patient_app'
  get '/appointments/app_check'
  get '/appointments/app_delete'
  get '/appointments/app_delete_final'
  get '/homepage/future_feature'


  resources :invoices

  resources :invoice_statuses

  resources :diag_codes

  resources :physicians

  resources :patients

  resources :app_statuses

  resources :apps

  resources :appointments

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'homepage#index'

  #route for doc_notes
  #match "appointments/:id/doc_notes" => "appointments#doc_notes"
  #get 'appointments/doc_notes', to: 'appointments#doc_notes'
  get '/appointments/doc_notes'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
