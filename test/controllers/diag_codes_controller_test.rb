require 'test_helper'

class DiagCodesControllerTest < ActionController::TestCase
  setup do
    @diag_code = diag_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:diag_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create diag_code" do
    assert_difference('DiagCode.count') do
      post :create, diag_code: { description: @diag_code.description, diag_code: @diag_code.diag_code }
    end

    assert_redirected_to diag_code_path(assigns(:diag_code))
  end

  test "should show diag_code" do
    get :show, id: @diag_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @diag_code
    assert_response :success
  end

  test "should update diag_code" do
    patch :update, id: @diag_code, diag_code: { description: @diag_code.description, diag_code: @diag_code.diag_code }
    assert_redirected_to diag_code_path(assigns(:diag_code))
  end

  test "should destroy diag_code" do
    assert_difference('DiagCode.count', -1) do
      delete :destroy, id: @diag_code
    end

    assert_redirected_to diag_codes_path
  end
end
