class Appointment < ActiveRecord::Base

  belongs_to :app_status
  belongs_to :patient
  belongs_to :physician
  belongs_to :diag_code
  has_one :invoice

  scope :physician, -> (physician_id) { where physician_id: physician_id }
  scope :patient, -> (patient_id) { where patient_id: patient_id }

  def app_info
    "#{app_date}:  #{patient.last_name}, #{patient.first_name}"
  end

end
