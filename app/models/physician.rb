class Physician < ActiveRecord::Base
  has_many :appointments

  #method for lastname, firstname

  def full_name
    "Dr. #{first_name} #{last_name}"
  end

end
