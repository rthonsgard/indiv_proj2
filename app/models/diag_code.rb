class DiagCode < ActiveRecord::Base

  has_many :appointments

  def code_value
    "#{diag_code}"
  end

end
