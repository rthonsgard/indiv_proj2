class Patient < ActiveRecord::Base

  has_many :appointments

  #method for lastname, firstname
  def full_name
    "#{last_name}, #{first_name}"
  end

end