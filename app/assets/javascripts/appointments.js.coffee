# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $("#datetimepicker").datetimepicker
    pickTime: true
    daysOfWeekDisabled: [
      0
      6
    ]
    minuteStepping: 30
    showToday: true
    sideBySide: true

  return
