json.array!(@physicians) do |physician|
  json.extract! physician, :id, :last_name, :first_name, :phone, :email
  json.url physician_url(physician, format: :json)
end
