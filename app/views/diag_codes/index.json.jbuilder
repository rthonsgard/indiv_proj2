json.array!(@diag_codes) do |diag_code|
  json.extract! diag_code, :id, :diag_code, :description
  json.url diag_code_url(diag_code, format: :json)
end
