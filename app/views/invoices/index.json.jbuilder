json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :appointment_id, :amount, :invoice_status_id
  json.url invoice_url(invoice, format: :json)
end
