json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :app_date, :reason, :dr_notes, :app_status_id, :diag_code_id
  json.url appointment_url(appointment, format: :json)
end
