json.array!(@patients) do |patient|
  json.extract! patient, :id, :last_name, :first_name, :phone, :email
  json.url patient_url(patient, format: :json)
end
