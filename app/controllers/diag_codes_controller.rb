class DiagCodesController < ApplicationController
  before_action :set_diag_code, only: [:show, :edit, :update, :destroy]

  # GET /diag_codes
  # GET /diag_codes.json
  def index
    @diag_codes = DiagCode.all
  end

  # GET /diag_codes/1
  # GET /diag_codes/1.json
  def show
  end

  # GET /diag_codes/new
  def new
    @diag_code = DiagCode.new
  end

  # GET /diag_codes/1/edit
  def edit
  end

  # POST /diag_codes
  # POST /diag_codes.json
  def create
    @diag_code = DiagCode.new(diag_code_params)

    respond_to do |format|
      if @diag_code.save
        format.html { redirect_to @diag_code, notice: 'Diag code was successfully created.' }
        format.json { render :show, status: :created, location: @diag_code }
      else
        format.html { render :new }
        format.json { render json: @diag_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diag_codes/1
  # PATCH/PUT /diag_codes/1.json
  def update
    respond_to do |format|
      if @diag_code.update(diag_code_params)
        format.html { redirect_to @diag_code, notice: 'Diag code was successfully updated.' }
        format.json { render :show, status: :ok, location: @diag_code }
      else
        format.html { render :edit }
        format.json { render json: @diag_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diag_codes/1
  # DELETE /diag_codes/1.json
  def destroy
    @diag_code.destroy
    respond_to do |format|
      format.html { redirect_to diag_codes_url, notice: 'Diag code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diag_code
      @diag_code = DiagCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diag_code_params
      params.require(:diag_code).permit(:diag_code, :description)
    end
end
